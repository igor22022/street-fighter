import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const firstFighterStartHealth = firstFighter.health;
    const secondFighterStartHealth = secondFighter.health;

    let firstFighterTempHealth = firstFighterStartHealth,
        firstFighterCurrentHealth = firstFighterStartHealth,
        secondFighterTempHealth = secondFighterStartHealth,
        secondFighterCurrentHealth = secondFighterStartHealth;

    window.addEventListener("keydown", keysPressed, false);
    window.addEventListener("keyup", keysReleased, false);

    var keys = [];

    function keysPressed(e) {
      keys[e.code] = true;
      
      if (keys[controls.PlayerOneAttack]) {
        secondFighterTempHealth = secondFighterCurrentHealth - getHitPower(firstFighter);
      } else if ((keys[controls.PlayerOneAttack] && keys[controls.PlayerTwoBlock]) ||
                (keys[controls.PlayerTwoBlock] && keys[controls.PlayerTwoAttack] && keys[controls.PlayerOneAttack])) {
        secondFighterTempHealth = secondFighterCurrentHealth - getDamage(firstFighter, secondFighter);
      } else if (keys[controls.PlayerTwoAttack]) {
        firstFighterTempHealth = firstFighterCurrentHealth - getHitPower(secondFighter);
      } else if ((keys[controls.PlayerTwoAttack] && keys[controls.PlayerOneBlock]) || 
                (keys[controls.PlayerOneBlock] && keys[controls.PlayerOneAttack] && keys[controls.PlayerTwoAttack])) {
        firstFighterTempHealth = firstFighterCurrentHealth - getDamage(secondFighter, firstFighter);
      } else if (keys[controls.PlayerOneCriticalHitCombination[0]] && keys[controls.PlayerOneCriticalHitCombination[1]] && 
                  keys[controls.PlayerOneCriticalHitCombination[2]]) {
        secondFighterTempHealth = secondFighterCurrentHealth - getHitPower(firstFighter) * 2;
      } else if (keys[controls.PlayerTwoCriticalHitCombination[0]] && keys[controls.PlayerTwoCriticalHitCombination[1]] && 
                  keys[controls.PlayerTwoCriticalHitCombination[2]]) {
        firstFighterTempHealth = firstFighterCurrentHealth - getHitPower(secondFighter) * 2;
      }

      if (firstFighterCurrentHealth !== firstFighterTempHealth) {
        firstFighterCurrentHealth = firstFighterTempHealth;
        let healthPercent = checkHelthIndication(firstFighterStartHealth, firstFighterCurrentHealth);
        let elem = document.getElementById('left-fighter-indicator');
        elem.style.width = healthPercent + '%';
      } else if (secondFighterCurrentHealth !== secondFighterTempHealth) {
        secondFighterCurrentHealth = secondFighterTempHealth;
        let healthPercent = checkHelthIndication(secondFighterStartHealth, secondFighterCurrentHealth);
        let elem = document.getElementById('right-fighter-indicator');
        elem.style.width = healthPercent + '%';
      }
  
      if (firstFighterCurrentHealth <= 0) {
        resolve(secondFighter);
      } else if (secondFighterCurrentHealth <= 0) {
        resolve(firstFighter);
      }
    }

    function keysReleased(e) {
      keys[e.code] = false;
    }

    function checkHelthIndication(startHealth, currentHealth) {
      return (currentHealth * 100) / startHealth;
    }
  })
}


export function getDamage(attacker, defender) {
  // return damage
  let damage = getBlockPower(defender) > getHitPower(attacker) ? 0 : (getHitPower(attacker) - getBlockPower(defender));

  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1; 
  const attack = fighter.attack;
  let power = attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance  = Math.random() + 1;
  const defense = fighter.defense;
  let power = defense * dodgeChance;

  return power;
}
