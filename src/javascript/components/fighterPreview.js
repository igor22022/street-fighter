import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterInfo = createFighterInfo(fighter);
    fighterElement.append(fighterImage, fighterInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterInfo(fighter) {
  const infoContainer = createElement({ tagName: 'div', className: 'info-wrapper' });
  let fighterInfo = [];
  Object.keys(fighter).forEach((key) => {
    if (key !== '_id' && key !== 'source') {
      let infoItem = createElement({ tagName: 'span', className: 'info-item' })
      infoItem.innerText = `${key}: ${fighter[key]} `
      fighterInfo.push(infoItem);
    }
  });
  infoContainer.append(...fighterInfo);

  return infoContainer;
}
