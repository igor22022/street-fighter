import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function 
  const title = `Winner is ${fighter.name}`;
  const bodyElement = createFighterImage(fighter);
  const onClose = () => { location.reload() };
  showModal({title, bodyElement, onClose});
}
